package d.kenin.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import d.kenin.model.Customer;
import d.kenin.repository.CustomerRepository;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test") // uses test configuration from: test/resources/application-test.properties
public class CustomerControllerTest {
    private static final String ERROR_MSG_IS_MANDATORY = "is mandatory";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CustomerRepository mockRepository;

    private String customerBaseRequestPath = "/v1/customers";

    private static final ObjectMapper om = new ObjectMapper();

    /**
     * Context:    /api
     * <p>POST:    /v1/customers
     * <p>Body:    {"name": "","surname": null,"country": "","email": "@mail.com","password": ""}
     * <p>Error response:
     * <p>{
     * <p>"timestamp": "2021-05-03T07:31:13.302+00:00",
     * <p>"status": 400,
     * <p>"error": "Bad Request",
     * <p>"message": "Validation failed for object='customer'. Error count: 5",
     * <p>"errors": {
     * <p>	"email": "must be a well-formed email address",
     * <p>	"password": "is mandatory",
     * <p>	"name": "is mandatory",
     * <p>	"surname": "is mandatory"
     * <p>  "country": "is mandatory"
     * <p>   },
     * <p>"path": "/v1/customers"
     * <p>}
     * @throws Exception
     */
    @Test
    public void createCustomer_BAD_REQUEST() throws Exception {
        mockMvc.perform(post(customerBaseRequestPath)
                .content(getInvalidInputs().get(0))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz")) //admin123
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.timestamp", is(notNullValue())))
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.message", is("Validation failed for object='customer'. Error count: 5")))
                .andExpect(jsonPath("$.errors").isMap())
                .andExpect(jsonPath("$.errors.name", is(ERROR_MSG_IS_MANDATORY)))
                .andExpect(jsonPath("$.errors.surname", is(ERROR_MSG_IS_MANDATORY)))
                .andExpect(jsonPath("$.errors.country", is(ERROR_MSG_IS_MANDATORY)))
                .andExpect(jsonPath("$.errors.email", is("must be a well-formed email address")))
                .andExpect(jsonPath("$.errors.password", is(ERROR_MSG_IS_MANDATORY)));

        verify(mockRepository, times(0)).save(any(Customer.class));
    }

    /**
     * Context:    /api
     * <p>GET:     /v1/customers/1
     * <p>Body:
     * <p>Response:
     * <p>{
     * <p>	"timestamp": "2021-05-03T19:13:27.786+00:00",
     * <p>	"status": 404,
     * <p>	"error": "Not Found",
     * <p>	"message": "The customer not found, requested id: 1",
     * <p>	"path": "/v1/customers/1"
     * <p>}
     * @throws Exception
     */
    @Test
    public void getCustomer_NOT_FOUND() throws Exception {
        // when
        mockMvc.perform(get(customerBaseRequestPath + "/1")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz"))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.timestamp", is(notNullValue())))
                .andExpect(jsonPath("$.status", is(404)))
                .andExpect(jsonPath("$.message", is("The customer not found, requested id: 1")));

        // then
        verify(mockRepository, times(1)).findById(any(Long.class));
    }

    /**
     * Context:    /api
     * <p>GET:     /v1/customers
     * <p>Body:
     * <p>Response:
     * <p>[
     * <p>       {
     * <p>		"id": 1,
     * <p>		"name": "Name_0",
     * <p>		"surname": "Surname_0",
     * <p>		"country": "Country_0",
     * <p>	"email": "email_0@mail.com",
     * <p>		"password": "pwd_0"
     * <p>   },
     * <p>   {
     * <p>		"id": 2,
     * <p>		"name": "Name_1",
     * <p>		"surname": "Surname_1",
     * <p>		"country": "Country_1",
     * <p>		"email": "email_1@mail.com",
     * <p>		"password": "pwd_1"
     * <p>   }
     * <p>]
     * @throws Exception
     */
    @Test
    public void getAllCustomers_SUCCESS() throws Exception {
        // given
        Customer inputData0 = getEntityInputFrom(getValidInputs().get(0));
        inputData0.setId(1);
        Customer inputData1 = getEntityInputFrom(getValidInputs().get(1));
        inputData1.setId(2);
        Mockito.when(mockRepository.findAll()).thenReturn(Lists.list(inputData0, inputData1));

        // when
        mockMvc.perform(get(customerBaseRequestPath)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$"). isArray())
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].name", is("Name_0")))
                .andExpect(jsonPath("$[0].surname", is("Surname_0")))
                .andExpect(jsonPath("$[0].country", is("Country_0")))
                .andExpect(jsonPath("$[0].email", is("email_0@mail.com")))
                .andExpect(jsonPath("$[0].password", is("pwd_0")))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].name", is("Name_1")))
                .andExpect(jsonPath("$[1].surname", is("Surname_1")))
                .andExpect(jsonPath("$[1].country", is("Country_1")))
                .andExpect(jsonPath("$[1].email", is("email_1@mail.com")))
                .andExpect(jsonPath("$[1].password", is("pwd_1")));

        // then
        verify(mockRepository, times(1)).findAll();
    }

    /**
     *    Context: /api
     * <p>POST:    /v1/customers
     * <p>Body:    {"name": "Name_0","surname": "Surname_0","country": "Country_0","email": "email_0@mail.com","password": "pwd_0"}
     * <p>Response: {"id":1}
     * @throws Exception
     */
    @Test
    public void createCustomer_SUCCESS() throws Exception {
        // given
        Customer inputData = getEntityInputFrom(getValidInputs().get(0));
        inputData.setId(1);
        Mockito.when(mockRepository.save(any(Customer.class))).thenReturn(inputData);

        // when
        mockMvc.perform(post(customerBaseRequestPath)
                .content(getValidInputs().get(0))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)));

        // then
        verify(mockRepository, times(1)).save(any(Customer.class));
    }

    /**
     *    Context: /api
     * <p>PUT:     /v1/customers/1
     * <p>Body:    {"name": "Name_1","surname": "Surname_1","country": "Country_1","email": "email_1@mail.com","password": "pwd_1"}
     * <p>Response: {"id":1}
     * @throws Exception
     */
    @Test
    public void updateCustomer_SUCCESS() throws Exception {
        // given
        Customer inputData0 = getEntityInputFrom(getValidInputs().get(0));
        inputData0.setId(1);
        Customer inputData1 = getEntityInputFrom(getValidInputs().get(1));
        inputData1.setId(1);
        Mockito.when(mockRepository.findById(1L)).thenReturn(Optional.of(inputData0));
        Mockito.when(mockRepository.save(any(Customer.class))).thenReturn(inputData1);

        // when
        mockMvc.perform(put(customerBaseRequestPath + "/1")
                .content(getValidInputs().get(1))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)));

        // then
        verify(mockRepository, times(1)).findById(any(Long.class));
        verify(mockRepository, times(1)).save(any(Customer.class));
    }

    /**
     *    Context: /api
     * <p>DELETE:  /v1/customers/1
     * <p>Body:
     * <p>Response: Delete - OK: 1
     * @throws Exception
     */
    @Test
    public void deleteCustomer_SUCCESS() throws Exception {
        // given
        Customer inputData1 = getEntityInputFrom(getValidInputs().get(1));
        inputData1.setId(1);
        Mockito.when(mockRepository.findById(1L)).thenReturn(Optional.of(inputData1));

        // when
        mockMvc.perform(delete(customerBaseRequestPath + "/1")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("Delete - OK: 1"));

        // then
        verify(mockRepository, times(1)).findById(any(Long.class));
        verify(mockRepository, times(1)).delete(any(Customer.class));
    }

    private List<String> getValidInputs() {
        return Lists.list(
                "{\"name\": \"Name_0\",\"surname\": \"Surname_0\",\"country\": \"Country_0\",\"email\": \"email_0@mail.com\",\"password\": \"pwd_0\"}",
                "{\"name\": \"Name_1\",\"surname\": \"Surname_1\",\"country\": \"Country_1\",\"email\": \"email_1@mail.com\",\"password\": \"pwd_1\"}"
        );
    }

    private List<String> getInvalidInputs() {
        return Lists.list(
                "{\"name\": \"\",\"surname\": null,\"country\": \"\",\"email\": \"@mail.com\",\"password\": \"\"}",
                "{\"name\": \"Name_1\",\"surname\": \"Surname_1\",\"country\": \"Country_1\",\"email\": null,\"password\": \"pwd_1\"}"
        );
    }

    private Customer getEntityInputFrom(String jsonInput) throws JsonProcessingException {
        return om.readerFor(Customer.class).readValue(jsonInput);
    }

}
