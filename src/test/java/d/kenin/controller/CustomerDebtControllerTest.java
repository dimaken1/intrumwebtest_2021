package d.kenin.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import d.kenin.model.Customer;
import d.kenin.model.CustomerDebt;
import d.kenin.repository.CustomerDebtRepository;
import d.kenin.repository.CustomerRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test") // uses test configuration from: test/resources/application-test.properties
public class CustomerDebtControllerTest {
    private static final String ERROR_MSG_IS_MANDATORY = "is mandatory";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CustomerDebtRepository mockRepository;

    @MockBean
    private CustomerRepository mockCustomerRepository;

    private String customerBaseRequestPath = "/v1/customers";

    private static final ObjectMapper om = new ObjectMapper();

    @BeforeAll
    public static void beforeAllTests() {
        om.registerModule(new JavaTimeModule());
    }

    @Test
    public void createDebt_BAD_REQUEST_on_validation() throws Exception {
        // given
        String input = "{\"amount\": \"0\",\"currency\": \"E\",\"dueDate\": \"\"}";

        // when
        mockMvc.perform(post(customerBaseRequestPath + "/1/debt")
                .content(input)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz"))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.timestamp", is(notNullValue())))
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.message", is("Validation failed for object='customerDebt'. Error count: 3")))
                .andExpect(jsonPath("$.errors").isMap())
                .andExpect(jsonPath("$.errors.amount", is("must be greater than 0")))
                .andExpect(jsonPath("$.errors.currency", is("the code must be 3 letters long")))
                .andExpect(jsonPath("$.errors.dueDate", is(ERROR_MSG_IS_MANDATORY)));

        verify(mockCustomerRepository, times(0)).findById(any(Long.class));
    }

    @Test
    public void createDebt_BAD_REQUEST_on_dueDate_validation() throws Exception {
        // given
        String input = "{\"amount\": \"200.50\",\"currency\": \"EUR\",\"dueDate\": \"2021-45\"}";

        // when
        mockMvc.perform(post(customerBaseRequestPath + "/1/debt")
                .content(input)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz"))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.timestamp", is(notNullValue())))
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.message", is("Validation failed. Error count: 1")))
                .andExpect(jsonPath("$.errors").isMap())
                .andExpect(jsonPath("$.errors.dueDate", is("has invalid format: 2021-45")));

        verify(mockCustomerRepository, times(0)).findById(any(Long.class));
    }

    @Test
    public void createDebt_NOT_FOUND_customer() throws Exception {
        // given
        String input = "{\"amount\": \"200.50\",\"currency\": \"EUR\",\"dueDate\": \"2021-05-03\"}";

        // when
        mockMvc.perform(post(customerBaseRequestPath + "/1/debt")
                .content(input)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz"))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.timestamp", is(notNullValue())))
                .andExpect(jsonPath("$.status", is(404)))
                .andExpect(jsonPath("$.message", is("The customer not found, requested id: 1")));

        // then
        verify(mockCustomerRepository, times(1)).findById(any(Long.class));
        verify(mockRepository, times(0)).findByCustomerId(any(Long.class));
        verify(mockRepository, times(0)).save(any(CustomerDebt.class));
    }

    @Test
    public void getDebt_SUCCESS() throws Exception {
        // given
        String input = "{\"id\": \"1\",\"amount\": \"200.50\",\"currency\": \"EUR\",\"dueDate\": \"2021-05-03\"}";
        CustomerDebt debt = getEntityInputFrom(input);
        Mockito.when(mockRepository.findByCustomerId(1L)).thenReturn(Optional.of(debt));

        // when
        mockMvc.perform(get(customerBaseRequestPath + "/1/debt")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.amount", is("200.50")))
                .andExpect(jsonPath("$.currency", is("EUR")))
                .andExpect(jsonPath("$.dueDate", is("2021-05-03")));

        // then
        verify(mockRepository, times(1)).findByCustomerId(any(Long.class));
        verify(mockCustomerRepository, times(0)).findById(any(Long.class));
    }

    @Test
    public void createDebt_SUCCESS() throws Exception {
        // given
        String input = "{\"amount\": \"200.50\",\"currency\": \"EUR\",\"dueDate\": \"2021-05-03\"}";
        Customer customer = new Customer();
        customer.setId(1);
        CustomerDebt debt = getEntityInputFrom(input);
        debt.setId(1);
        Mockito.when(mockCustomerRepository.findById(1L)).thenReturn(Optional.of(customer));
        Mockito.when(mockRepository.findByCustomerId(1L)).thenReturn(Optional.of(debt));
        Mockito.when(mockRepository.save(any(CustomerDebt.class))).thenReturn(debt);

        // when
        mockMvc.perform(post(customerBaseRequestPath + "/1/debt")
                .content(input)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)));

        // then
        verify(mockCustomerRepository, times(1)).findById(any(Long.class));
        verify(mockRepository, times(1)).findByCustomerId(any(Long.class));
        verify(mockRepository, times(1)).save(any(CustomerDebt.class));
    }

    /**
     *    Context: /api
     * <p>DELETE:  /v1/customers/1/debt
     * <p>Body:
     * <p>Response: Delete - OK: 1
     * @throws Exception
     */
    @Test
    public void deleteDebt_SUCCESS() throws Exception {
        // given
        CustomerDebt debt = new CustomerDebt();
        debt.setId(1);
        Mockito.when(mockRepository.findByCustomerId(1L)).thenReturn(Optional.of(debt));

        // when
        mockMvc.perform(delete(customerBaseRequestPath + "/1/debt")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("Delete - OK: 1"));

        // then
        verify(mockCustomerRepository, times(0)).findById(any(Long.class));
        verify(mockRepository, times(1)).findByCustomerId(any(Long.class));
        verify(mockRepository, times(1)).delete(any(CustomerDebt.class));
    }

    private CustomerDebt getEntityInputFrom(String jsonInput) throws JsonProcessingException {
        return om.readerFor(CustomerDebt.class).readValue(jsonInput);
    }

}
