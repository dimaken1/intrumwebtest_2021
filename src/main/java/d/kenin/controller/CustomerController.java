package d.kenin.controller;

import d.kenin.model.Customer;
import d.kenin.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/v1/customers")
public class CustomerController {

    @Autowired
    private CustomerService service;

    @GetMapping("")
    public List<Customer> getAll() {
        return service.getAllCustomers();
    }

    @GetMapping("{id}")
    public Customer getById(@PathVariable("id") long id) {
        return service.getCustomerById(id);
    }

    @PostMapping("")
    public Map<String, Long> create(@Valid @RequestBody Customer customer) {
        long id = service.createCustomer(customer);
        Map<String, Long> result = new HashMap<>();
        result.put("id", id);
        return result;
    }

    @PutMapping("{id}")
    public Map<String, Long> update(@PathVariable("id") long id, @Valid @RequestBody Customer data) {
        service.updateCustomer(id, data);
        Map<String, Long> result = new HashMap<>();
        result.put("id", id);
        return result;
    }

    @DeleteMapping("{id}")
    public String delete(@PathVariable("id") long id) {
        service.deleteCustomer(id);
        return "Delete - OK: " + id;
    }

}
