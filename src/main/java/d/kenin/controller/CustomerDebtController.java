package d.kenin.controller;

import d.kenin.model.CustomerDebt;
import d.kenin.service.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/v1/customers/{id}/debt")
public class CustomerDebtController {
    private static final Logger log = LoggerFactory.getLogger(CustomerDebtController.class);

    @Autowired
    private CustomerService service;

    @GetMapping("")
    public CustomerDebt get(@PathVariable("id") long customerId) {
        return service.getAllDebt(customerId);
    }

    @PostMapping("")
    public Map<String, Long> createOrUpdate(@PathVariable("id") long customerId, @Valid @RequestBody CustomerDebt data) {
        long id = service.createOrUpdateDebt(customerId, data);
        Map<String, Long> result = new HashMap<>();
        result.put("id", id);
        return result;
    }

    @DeleteMapping("")
    public String delete(@PathVariable("id") long customerId) {
        Optional<Long> opDeletedId = service.deleteDebt(customerId);
        String deletedId = "";
        if (opDeletedId.isPresent()) {
            deletedId = ": " + opDeletedId.get();
        }
        return "Delete - OK" + deletedId;
    }

}
