package d.kenin.service;

import d.kenin.exception.ResourceNotFoundException;
import d.kenin.model.Customer;
import d.kenin.model.CustomerDebt;
import d.kenin.repository.CustomerDebtRepository;
import d.kenin.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerService {
    public static final String ERROR_MSG_CUSTOMER_NOT_FOUND = "The customer not found, requested id: ";

    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private CustomerDebtRepository debtRepository;

    public List<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }

    public Customer getCustomerById(long id) {
        return customerRepository.findById(id).orElseThrow(() ->
                new ResourceNotFoundException(ERROR_MSG_CUSTOMER_NOT_FOUND + id));
    }

    public long createCustomer(Customer customer) {
        customer = customerRepository.save(customer);
        return customer.getId();
    }

    public void updateCustomer(long id, Customer data) {
        Customer customer =  customerRepository.findById(id).orElseThrow(() ->
                new ResourceNotFoundException(ERROR_MSG_CUSTOMER_NOT_FOUND + id));
        customer.setName(data.getName());
        customer.setSurname(data.getSurname());
        customer.setCountry(data.getCountry());
        customer.setEmail(data.getEmail());
        customer.setPassword(data.getPassword());
        customerRepository.save(customer);
    }

    public void deleteCustomer(long id) {
        Customer customer =  customerRepository.findById(id).orElseThrow(() ->
                new ResourceNotFoundException(ERROR_MSG_CUSTOMER_NOT_FOUND + id));
        customerRepository.delete(customer);
    }

    // Debt part
    public CustomerDebt getAllDebt(long customerId) {
        return debtRepository.findByCustomerId(customerId).orElse(null);
    }

    public long createOrUpdateDebt(long customerId, CustomerDebt data) {
        Customer customer = customerRepository.findById(customerId).orElseThrow(() ->
                new ResourceNotFoundException(ERROR_MSG_CUSTOMER_NOT_FOUND + customerId));
        CustomerDebt debt = debtRepository.findByCustomerId(customerId).orElse(null);
        if (debt == null) {
            debt = data;
            debt.setCustomer(customer);
        } else {
            debt.setAmount(data.getAmount());
            debt.setCurrency(data.getCurrency());
            debt.setDueDate(data.getDueDate());
        }
        debt = debtRepository.save(debt);
        return debt.getId();
    }

    public Optional<Long> deleteDebt(long customerId) {
        CustomerDebt debt = debtRepository.findByCustomerId(customerId).orElse(null);
        Long id = null;
        if (debt != null) {
            id = debt.getId();
            debtRepository.delete(debt);
        }
        return Optional.of(id);
    }

}
