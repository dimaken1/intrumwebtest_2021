package d.kenin.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
@Entity
@Table(name = "customer")
public class Customer {
    private static final String ERROR_MSG_IS_MANDATORY = "is mandatory";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotBlank(message = ERROR_MSG_IS_MANDATORY)
    @Column(nullable = false, length = 150)
    private String name;

    @NotBlank(message = ERROR_MSG_IS_MANDATORY)
    @Column(nullable = false, length = 150)
    private String surname;

    @NotBlank(message = ERROR_MSG_IS_MANDATORY)
    @Column(nullable = false, length = 60)
    private String country;

    @NotBlank(message = ERROR_MSG_IS_MANDATORY)
    @Email
    @Column(nullable = false, length = 150)
    private String email;

    @NotBlank(message = ERROR_MSG_IS_MANDATORY)
    @Column(nullable = false)
    private String password;

}
