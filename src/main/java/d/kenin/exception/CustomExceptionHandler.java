package d.kenin.exception;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
        Map<String, String> errors = new LinkedHashMap<>();
        ex.getBindingResult().getFieldErrors().forEach(error ->
                errors.put(error.getField(), error.getDefaultMessage()));

        String message = "Validation failed for object='" + ex.getObjectName() + "'. Error count: " +  errors.size();
        return buildExceptionResponse(
                headers, HttpStatus.BAD_REQUEST, request, message, errors);
    }

    @ExceptionHandler({ResourceNotFoundException.class, InvalidFormatException.class})
    public ResponseEntity<Object> handleExceptionsForCustomization(Exception ex,
                                                                   WebRequest request) throws Exception {
        HttpHeaders headers = new HttpHeaders();
        HttpStatus newStatus = HttpStatus.BAD_REQUEST;
        String message = null;
        Map<String, String> errors = new LinkedHashMap<>();
        if (ex instanceof ResourceNotFoundException) {
            newStatus = HttpStatus.NOT_FOUND;
            message = ex.getMessage();
        } else if (ex instanceof InvalidFormatException) {
            InvalidFormatException fex = (InvalidFormatException) ex;
            List<JsonMappingException.Reference> refs = fex.getPath();
            refs.forEach(ref ->
                    errors.put(ref.getFieldName(), "has invalid format: " + fex.getValue()));
            message = "Validation failed. Error count: " +  errors.size();
        } else {
            throw ex;
        }
        return buildExceptionResponse(headers, newStatus, request, message, errors);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
        ResponseEntity<Object> exceptionResponse;
        if (ex.contains(InvalidFormatException.class)) {
            InvalidFormatException ifex = null;
            Throwable cause = ex.getCause();
            while(cause != null) {
                if (InvalidFormatException.class.isInstance(cause)) {
                    ifex = (InvalidFormatException) cause;
                    break;
                }
                if (cause.getCause() == cause) {
                    break;
                }
                cause = cause.getCause();
            }
            if (ifex != null) {
                try {
                    exceptionResponse = handleExceptionsForCustomization(ifex, request);
                } catch (Exception e) {
                    exceptionResponse = super.handleHttpMessageNotReadable(ex, headers, status, request);
                }
            } else {
                exceptionResponse = super.handleHttpMessageNotReadable(ex, headers, status, request);
            }

        } else {
            exceptionResponse = super.handleHttpMessageNotReadable(ex, headers, status, request);;
        }

        return exceptionResponse;
    }

    protected ResponseEntity<Object> buildExceptionResponse(HttpHeaders headers,
                                                            HttpStatus status,
                                                            WebRequest request,
                                                            String message) {
        return buildExceptionResponse(headers, status, request, message, null);
    }

    protected ResponseEntity<Object> buildExceptionResponse(HttpHeaders headers,
                                                            HttpStatus status,
                                                            WebRequest request,
                                                            String message,
                                                            Map<String, String> errors) {
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", new Date());
        body.put("status", status.value());
        body.put("error", status.getReasonPhrase());
        body.put("message", message);
        if (errors != null && !errors.isEmpty()) {
            body.put("errors", errors);
        }
        body.put("path", ((ServletWebRequest)request).getRequest().getRequestURI().toString());
        return new ResponseEntity<>(body, headers, status);
    }

}
