package d.kenin.repository;

import d.kenin.model.CustomerDebt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface CustomerDebtRepository extends JpaRepository<CustomerDebt, Long> {

    Optional<CustomerDebt> findByCustomerId(long id);

    /**
     * Delete customer debt by customer id.
     * <p>This is transactional operation.
     *
     * @param id customer ID
     */
    @Transactional
    void deleteByCustomerId(long id);
}
